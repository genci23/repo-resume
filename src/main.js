import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";
import { createStore } from "vuex";

import App from "./App.vue";
import TitleDocuments from "./components/TitleDocuments.vue";
import BaseWrapper from "./components/BaseWrapper.vue";
import InputUser from "./components/InputUser.vue";
import UserResume from "./components/UserResume.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", component: InputUser },
    {
      path: "/:userName",
      component: UserResume,
    },
  ],
});

const store = createStore({
  state() {
    return {
      enteredUser: "",
    };
  },
});

const app = createApp(App);

app.component("title-documents", TitleDocuments);
app.component("base-wrapper", BaseWrapper);

app.use(store);

app.use(router);

app.mount("#app");
